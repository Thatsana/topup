import { Component } from '@angular/core';


@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  color = [{
    tabOne: '',
    tabTwo: '',
    tabThree: '',
  }];
  constructor() { }

  colorchang(value) {
    if (value == 1) {
      this.color[0].tabOne = '#d7d8da';
      this.color[0].tabTwo = 'white';
      this.color[0].tabThree = 'white';
    }
    else if (value == 2) {
      this.color[0].tabOne = 'white';
      this.color[0].tabTwo = '#d7d8da';
      this.color[0].tabThree = 'white';
    }
    else if (value == 3) {
      this.color[0].tabOne = 'white';
      this.color[0].tabTwo = 'white';
      this.color[0].tabThree = '#d7d8da';
    }
    console.log(value)
    console.log(this.color[0])
  }
}
