
import { HttpClient } from '@angular/common/http';
import { Component, ViewChild } from '@angular/core';
import { ModalController, IonInfiniteScroll } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Modaltab1Page } from './modaltab1/modaltab1.page';
import 'rxjs/add/operator/map';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  dataPack: any;
  items: any;
  item = [];
  posts: any;

  constructor(
    private callNumber: CallNumber,
    private http: HttpClient,
    public modalController: ModalController) {
    this.http.get('https://www.reddit.com/r/gifs/new/.json?limit=13').subscribe(data => {
      //this.posts = data.data.children
      console.log(data);
    });

  }

  // Url = 'https://jsonplaceholder.typicode.com';
  ngOnInit() {
    return new Promise(resolve => {
      this.http.get('https://topup.toonoo.co/web/package-json').subscribe(data => {
        resolve(data);
        this.dataPack = data;
      }, err => {
        console.log(err);
      });
    });
  }

  call(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  async openModalTab1(value) {
    const modal = await this.modalController.create({
      component: Modaltab1Page,
      componentProps: {
        id: value
      }
    });
    await modal.present();
    console.log(value);
  }


}


