import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-modaltab1',
  templateUrl: './modaltab1.page.html',
  styleUrls: ['./modaltab1.page.scss'],
})
export class Modaltab1Page implements OnInit {
  create(arg0: { component: typeof Modaltab1Page; componentProps: { id: any; }; }) {
    throw new Error("Method not implemented.");
  }
  id = Number;

  constructor(
    public modalcontroller: ModalController,
    private callNumber: CallNumber
  ) { }
  call(number) {
    this.callNumber.callNumber(number, true)
      .then(res => console.log('Launched dialer!', res))
      .catch(err => console.log('Error launching dialer', err));
  }

  ngOnInit() {
    console.log(this.id)
  }
  closeModal() {
    console.log()
    this.modalcontroller.dismiss();
  }

}
